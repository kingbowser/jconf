package pw.bowser.jconf.impl;

import java.util.Map;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**
 * Implementation of Configuration that uses java serialization as a backend
 *
 * @author King Bowser <king@bowser.pw>
 */
public final class JObjectStreamConfiguration extends AbstractConfiguration {

	private final short CONFIG_VERSION = 0x01;

	public void writeTo(OutputStream stream) throws IOException {
		ObjectOutputStream serializer = new ObjectOutputStream(stream);
		serializer.writeShort(CONFIG_VERSION);
		serializer.writeObject(getPropertyMap());
		serializer.flush();
		serializer.close();
	}

	public void readFrom(InputStream stream) throws IOException {
		ObjectInputStream deserializer = new ObjectInputStream(stream);
		if(deserializer.readShort() != CONFIG_VERSION) {
			deserializer.close();
			throw new IOException("Read configuration with a different serial version than the implementation");
		} else {
			try (ObjectInputStream str = deserializer) {
				//noinspection unchecked
				Map<String, Object> stored = (Map<String, Object>) deserializer.readObject();
				deserializer.close();
				getPropertyMap().clear();
				getPropertyMap().putAll(stored);
			} catch(ClassNotFoundException cne) {
				throw new IOException("Deserialization error", cne);
			} catch(ClassCastException cce) {
				throw new IOException("Invalid object read during deserialization", cce);
			}
		}
	}

}	
