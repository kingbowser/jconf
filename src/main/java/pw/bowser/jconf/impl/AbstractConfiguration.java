package pw.bowser.jconf.impl;

import pw.bowser.jconf.Configuration;

import java.util.Optional;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Internal implementation of a configuration that is implemented by the different storage methods in this package
 *
 * @author King Bowser <king@bowser.pw>
 */
abstract class AbstractConfiguration implements Configuration {

	private final Map<String, Object> properties =
		Collections.synchronizedMap(new HashMap<>());

	protected Map<String, Object> getPropertyMap() {
		return properties;
	}

	public final boolean contains(String key) {
		return getPropertyMap().containsKey(key);
	}

	public final <T> Optional<T> remove(String key) {
        //noinspection unchecked
		return Optional.of((T) getPropertyMap().remove(key));
	}

	public final void set(String key, Object value) {
		getPropertyMap().put(key, value);
	}

	public final boolean setDefault(String key, Object value) {
		if(contains(key)) {
			return false;
		} else {
			set(key, value);
			return true;
		}
	}

	public final <T> Optional<T> get(String key) {
        //noinspection unchecked
		return Optional.of((T) getPropertyMap().get(key));
	}

	public final <T> T get(String key, T fallback) {
		return this.<T>get(key).orElse(fallback);
	}

}
