package pw.bowser.jconf;

import java.util.Optional;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**
 * Configuration interface provides that basic contract for a configuration.
 *
 * This configuration can store any arbitrary object in relation to a String value (key), which may then be stored
 * and loaded from streams.
 *
 * It provides useful logic helpers such as setDefault and get(key, fallback)
 *
 * @author King Bowser <bowser@hysteria.pw>
 */
public interface Configuration {

	/**
	 * Determine whether or not the configuration contains a given key
	 *
	 * @param key name of property
	 * @return true when `key` has a corresponding value, otherwise false
	 */
	boolean contains(String key);

	/**
	 * Remove a property `key` and return an optional wrapped value corresponding the the value of
	 * the property prior to removal
	 *
	 * @param key name of property
	 * @return Option(value) if `key` corresponded to a value, otherwise Optionalempty[T]
	 */
	<T> Optional<T> remove(String key);

	/**
	 * Set a value for `key`, discarding any previous value
	 *
	 * @param key name of property
	 * @param value contents of property
	 */
	void set(String key, Object value);

	/**
	 * Set a value for `key` provided no value already exists
	 *
	 * @param key name of property
	 * @param value contents of property
	 * @see #set(String, Object)
	 * @return true if the value was set, otherwise false
	 */
	boolean setDefault(String key, Object value);

	/**
	 * Retrieve the value for `key`
	 * If the value is available, return Option(value) otherwise Optionalempty[T]
	 *
	 * @return value wrapped in optional
	 */
	<T> Optional<T> get(String key);

	/**
	 * Retrieves the value for `key`
	 * If `key` has a corresponding value, return that value
	 * Otherwise, return fallback
	 *
	 * @return value for `key`, otherwise `fallback`
	 */
	<T> T get(String key, T fallback);

	/**
	 * Write the configuration to an outputstream.
	 *
	 * Configuration format is arbitrary.
	 *
	 * @param stream stream to which the configuration will be written
	 */
	void writeTo(OutputStream stream) throws IOException;

	/**
	 * Read an inputstream and attempt to load a configuration therefrom.
	 *
	 * @param stream stream from which to read new data.
	 * @throws IOException when given a bad stream, the configuration should not have been modified
	 */
	void readFrom(InputStream stream) throws IOException;

}

